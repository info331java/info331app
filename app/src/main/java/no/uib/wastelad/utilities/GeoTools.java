package no.uib.wastelad.utilities;

import android.location.Location;

/**
 * Tools related to geographic locations
 *
 * @version 1.0
 * @author Rune Myklevoll
 * @since 03.12.2017
 */

public abstract class GeoTools {

    /**
     * Creates a location object based on
     * @param longitude longitude of the location
     * @param latitude latitude of the location
     * @return the location
     */
    public static Location buildLocation(float longitude, float latitude) {
        Location location = new Location("");
        location.setLongitude(longitude);
        location.setLatitude(latitude);
        return location;
    }

}
