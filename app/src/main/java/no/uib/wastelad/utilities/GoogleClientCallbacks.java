package no.uib.wastelad.utilities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Class contains callback methods for the google client api. Will be implemented when the business logic
 * behind error handling is sorted. (Should player be able to play without geolocations etc).
 *
 * @version 1.0
 * @author Rune Myklevoll
 * @since 03.12.2017
 */

public class GoogleClientCallbacks implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
