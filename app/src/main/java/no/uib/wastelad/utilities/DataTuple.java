package no.uib.wastelad.utilities;

/**
 * A tuple implementation used to store steps and timestamps from step sensor events.
 * @version 1.0
 * @author Adrian Borgund
 * @since 13.11.2017
 */

public class DataTuple {
    private long timestamp;
    private float steps;

    public DataTuple(long timestamp, float steps) {
        setTimestamp(timestamp);
        setSteps(steps);
    }

    public void setSteps(float steps) {
        if(steps > 0)
        this.steps = steps;
    }

    public void setTimestamp(long timestamp){
        if(timestamp > 0)
        this.timestamp = timestamp;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public float getSteps() {
        return steps;
    }
}
