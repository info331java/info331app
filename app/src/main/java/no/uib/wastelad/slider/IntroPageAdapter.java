package no.uib.wastelad.slider;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import no.uib.wastelad.R;

/**
 *
 * @author Felipe Sepulveda
 * @version 1.0
 *
 * IntroPageAdapter class adds the resource from the slides intro an array. The slides gets
 * pulled compare to the view.
 */

public class IntroPageAdapter extends android.support.v4.view.PagerAdapter
{
    private int[] layouts = {R.layout.slide_first,R.layout.slide_second,R.layout.slide_third,R.layout.slide_fourth, R.layout.slide_fifth, R.layout.slide_sixth};
    private final LayoutInflater layoutInflater;

    /**
     *
     * Initiates the XML file to the corresponding context.
     * @param context Argument context.
     */
    public IntroPageAdapter(Context context)
    {
        layoutInflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     *
     * Returns the number of layouts.
     * @return layout length
     */
    @Override
    public int getCount()
    {
        return layouts.length;
    }


    /**
     *
     * Return when the view is equals the object.
     * @param view current view
     * @param object current object
     * @return view equals object
     */
    @Override
    public boolean isViewFromObject(View view, Object object)
    {
        return view == object;
    }

    /**
     *
     * Initiates and inflates layouts.
     * @param container
     * @param position
     * @return
     */
    @Override
    public Object instantiateItem(ViewGroup container, int position)
    {
    View view = layoutInflater.inflate(layouts[position], container, false);
        container.addView(view);
        return view;
    }

    /**
     *
     * Method to destroy and remove view items.
     * @param container
     * @param position
     * @param object
     */
    @Override
    public void destroyItem(ViewGroup container, int position, Object object)
    {
        View view = (View)object;
        container.removeView(view);
    }
}