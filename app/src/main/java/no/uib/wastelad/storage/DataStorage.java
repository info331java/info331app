package no.uib.wastelad.storage;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

import no.uib.wastelad.elements.Base;
import no.uib.wastelad.engine.BaseController;
import no.uib.wastelad.engine.ModuleController;
import no.uib.wastelad.elements.Module;
import no.uib.wastelad.player.Pedometer;
import no.uib.wastelad.player.Player;

/**
 * Data storage class for the application used to save and load the game elements.
 * @version 1.0
 * @author Adrian Borgund
 * @since 03.11.2017
 */
public class DataStorage {
   private Context context;
    private SharedPreferences sharedPreferences;
    private Player player = Player.getInstance();
    private Pedometer pedometer = Pedometer.getInstance();
    private boolean loaded;

    public DataStorage(Context contextIn){
        this.context = contextIn;
        sharedPreferences = context.getSharedPreferences("gameStorage", context.MODE_PRIVATE);
    }

    /**
     * A method to save the user preference for showing the tutorial.
     * @param ignore boolean value, true to ignore tutorial
     */
    public void saveWelcomeScreenIgnore(boolean ignore){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("neverShow", ignore);
        editor.apply();
    }

    public boolean isWelcomeScreenIgnored(){
        return sharedPreferences.getBoolean("neverShow",false);
    }

    /**
     * A method to load the game state as a whole. Loading the values stored for player, pedometer, modules and bases.
     */
    public void loadGame() {
        if(loaded) return;
        loadPlayer();
        loadPedometer();
        loadModules();
        loadBases();
        loaded = true;
    }

    /**
     * A method to save the game as a whole with player, bases, modules and pedometer.
     * It is saved to the sharedPreferences in local storage.
     */
    public void saveGame() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        ArrayList<Module> modules = ModuleController.getInstance().getModules();
        ArrayList<Base> bases = BaseController.getInstance().getBases();
        savePlayer(editor, player);
        saveBases(editor, bases);
        saveModules(editor, modules);
        savePedometer(editor);
        editor.apply();
    }

    private void savePlayer(SharedPreferences.Editor editor, Player player) {
        editor.putLong("playerCurrency", player.getCurrency());
        editor.putFloat("playerSteps", player.getSteps());
        editor.putLong("timestampSave", System.currentTimeMillis());
    }

    private void saveBases(SharedPreferences.Editor editor, ArrayList<Base> bases) {
        for(int i = 0; i < bases.size(); i++) {
            Base base = bases.get(i);
            int[] baseData = base.saveBaseData();
            editor.putInt(i+"baseAccumulated", baseData[0]);
            editor.putInt(i+"baseStorageLevel", baseData[1]);
            editor.putInt(i+"baseProgressLevel", baseData[2]);
            editor.putInt(i+"baseUnlocked",baseData[3]);
        }
    }

    private void savePedometer(SharedPreferences.Editor editor) {
        long timeStamp = pedometer.getLastTimestamp();
        float totalSteps = pedometer.getTotalSteps();
        float lastSteps = pedometer.getLastSteps();
        if(timeStamp > 0) {
            editor.putLong("lastSavedTimestamp", timeStamp);
        }
        if(totalSteps > 0) {
            editor.putFloat("lastSavedTotalSteps", totalSteps);
        }
        if(lastSteps > 0) {
            editor.putFloat("lastSavedLastSteps", lastSteps);
        }
    }

    private void saveModules(SharedPreferences.Editor editor, ArrayList<Module> modules) {
        for(int i = 0; i < modules.size(); i++) {
            Module module = modules.get(i);
            int[] moduleData = module.saveModuleData();
            editor.putInt(i+"moduleStepProgress",moduleData[0]);
            editor.putInt(i+"moduleLevel",moduleData[1]);
            editor.putInt(i+"moduleUnlocked",moduleData[2]);
        }
    }

    /**
     * A method to get the time difference in milliseconds between last lave and current time.
     * @return long time difference between saving and loading, 0 if there is no save stored.
     */
    public long getSaveLoadTimeDifference() {
        long timestamp = System.currentTimeMillis();
        long savedTimestamp = sharedPreferences.getLong("timestampSave", 0);
        if(savedTimestamp > 0) {
            return timestamp - savedTimestamp;
        }
        return 0;
    }


    private void loadPlayer() {
        long playerCurrency = sharedPreferences.getLong("playerCurrency", 0);
        float playerSteps = sharedPreferences.getFloat("playerSteps", 0);
        if(playerSteps > 0) {
            player.setSteps(playerSteps);
        }
        if(playerCurrency > 0) {
            player.setCurrency(playerCurrency);
        }
    }

    private void loadPedometer() {
        if(pedometer != null) {
            long loadedTimestamp = sharedPreferences.getLong("lastSavedTimestamp", 0);
            float loadedTotalSteps = sharedPreferences.getFloat("lastSavedTotalSteps", 0);
            float loadedLastSteps = sharedPreferences.getFloat("lastSavedLastSteps", 0);

            if (loadedTimestamp > 0) {
                pedometer.setLastTimestamp(loadedTimestamp);

            }
            if(loadedTotalSteps > 0) {
                pedometer.setTotalSteps(loadedTotalSteps);

            }
            if(loadedLastSteps > 0) {
                pedometer.setLastSteps(loadedLastSteps);
                pedometer.setSavedSteps(loadedLastSteps);
            }
        }
    }

    private void loadModules() {
      ArrayList<Module> modules = ModuleController.getInstance().getModules();
      for(int i = 0; i < modules.size(); i++) {
          Module module = modules.get(i);
          int steps = sharedPreferences.getInt(i+"moduleStepProgress", 0);
          int level = sharedPreferences.getInt(i+"moduleLevel",0);
          int unlockedInt = sharedPreferences.getInt(i+"moduleUnlocked", 0);
          boolean unlocked = unlockedInt == 1;
          if(unlocked)
              module.loadModule(steps, level, unlocked);
        }
    }

    private void loadBases() {
        ArrayList<Base> bases = BaseController.getInstance().getBases();
        long awayTime = getSaveLoadTimeDifference();
        for(int i = 0; i < bases.size(); i++) {
            Base base = bases.get(i);
            int baseAccumulated = sharedPreferences.getInt(i+"baseAccumulated",0);
            int baseStorageLevel = sharedPreferences.getInt(i+"baseStorageLevel",0);
            int baseProgressLevel = sharedPreferences.getInt(i+"baseProgressLevel",0);
            int unlockedInt = sharedPreferences.getInt(i+"baseUnlocked",0);
            boolean baseUnlocked = unlockedInt == 1;
            if(awayTime > 10000 && awayTime + baseAccumulated < Integer.MAX_VALUE) {
                baseAccumulated += (int) (awayTime / 1000);
            }
            if(baseUnlocked)
                base.loadBase(baseAccumulated, baseStorageLevel, baseProgressLevel, baseUnlocked);
        }
    }
}

