package no.uib.wastelad.engine;

/**
 * Main engine holds the different engines that are included in the game.
 *
 * @version 1.0
 * @author Rune Myklevoll
 * @since 03.12.2017
 */

public class MainEngine {
    private static final MainEngine ourInstance = new MainEngine();
    private final TimedEngine timedEngine;
    private boolean running;

    /**
     * Constructor for the main engine
     */
    private MainEngine() {
        IController controller = BaseController.getInstance();
        timedEngine = new TimedEngine(controller);
    }

    /**
     * Start the engine
     */
    public void start() {
        if(!running) {
            timedEngine.startTimedEngine();
            running = true;
        }

    }

    public static MainEngine getInstance() {
        return ourInstance;
    }

}
