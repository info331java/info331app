package no.uib.wastelad.engine;

import android.os.Handler;

/**
 * Timed engine for the application that runs on a separate thread. The engine is responsible for
 * notifying the base controller to increase the base values, but can also be used for other time based
 * operations later.
 *
 * @version 1.0
 * @author Rune Myklevoll
 * @since 03.12.2017
 */
class TimedEngine {
    private final IController controller;
    private static final Handler handler = new Handler();
    private Runnable runnable;

    /**
     * Constructor for the TimedEngined class
     * @param controller the controller of the bases
     */
    public TimedEngine(IController controller) {
        this.controller = controller;
        createRunnable();
    }

    /**
     * Sets up the runnable operation
     */
    private void createRunnable() {
        this.runnable = new Runnable() {
            @Override
            public void run() {
                ((BaseController) controller).incrementAll();
                handler.postDelayed(runnable, 1000);
            }
        };
    }

    /**
     * Starts the runnable on the handler.
     */
    public void startTimedEngine() {
        handler.post(runnable);
    }
}
