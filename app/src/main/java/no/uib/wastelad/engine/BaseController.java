package no.uib.wastelad.engine;

import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

import no.uib.wastelad.R;
import no.uib.wastelad.activities.BasesActivity;
import no.uib.wastelad.elements.Base;
import no.uib.wastelad.elements.RequirementsNotMetException;
import no.uib.wastelad.player.Player;

/**
 * Controller for the bases in the application. Class serves as a entry point for the views and engines to access the bases.
 *
 * @version 1.0
 * @author Rune Myklevoll
 * @since 03.12.2017
 */

public class BaseController implements IController {
    private static BaseController ourInstance;
    private final ArrayList<Base> bases;
    private BasesActivity activity;

    /**
     * Constructor for the BaseController. Private due to singleton pattern.
     */
    private BaseController() {
        this.bases = new ArrayList<>();
        createBases();
    }

    /**
     * Attempt to upgrade an base. An exception is caught if the player is too far away
     * or cant afford the upgrade. The activity will show a proper response in such events.
     * @param base base to be upgraded
     * @param upgradeType which extension to upgrade
     */
    public void upgradeElement(Base base, int upgradeType){
        try{
            base.upgrade(upgradeType, activity.getLastKnownLocation());
            refreshAll();
        } catch (RequirementsNotMetException e) {
            activity.showRequirement(e);
        }
    }

    /**
     * Increment all bases
     */
    public void incrementAll() {
        for(Base base : bases){
            base.increment();
        }
    }

    /**
     * Decides what to do when the progressbar is clicked within the base view.
     * @param base the clicked base
     */
    public void progressBarAction(Base base) {
        if(base.isUnlocked()) {
            collectFrom(base);
        }
        else {
            unlock(base);
        }
        refreshAll();
    }

    /**
     * Collect accumulated karma from a given base and add to player currency
     * @param base base selected
     */
    private void collectFrom(Base base) {
        int collected = base.collect();
        Player.getInstance().addCurrency(collected);
    }

    /**
     * Attempt to unlock base
     * @param base base selected
     */
    private void unlock(Base base) {
        try {
            base.unlock(activity.getLastKnownLocation());
        } catch (RequirementsNotMetException e) {
            activity.showRequirement(e);
        }
    }

    /**
     * Refresh all base views
     */
    private void refreshAll() {
        for(Base base : bases) {
            base.notifyChange();
        }
    }

    private void createBases() {

        bases.add(new Base("Kiwi Krohnstad",1200, true,
                5.348757f, 60.375395f, R.drawable.truck, R.drawable.truckl));
        bases.add(new Base("SV-bygget", 1800,  false,
                5.324084f, 60.388640f, R.drawable.factory, R.drawable.factorylocked));
        bases.add(new Base("Ulriken", 2700, false,
                5.387038f, 60.377401f, R.drawable.boat, R.drawable.boatlocked));
        bases.add(new Base("Flesland",3600, false,
                5.222017f, 60.291819f, R.drawable.rocket, R.drawable.rocketlocked));
        bases.add(new Base("Godt brød Marken", 7200, false,
                5.329966f, 60.392219f, R.drawable.portal, R.drawable.portall));
    }

    public void setActivity(AppCompatActivity activity) {
        this.activity = (BasesActivity) activity;
    }


    public ArrayList<Base> getBases() {
        return bases;
    }

    public static BaseController getInstance() {
        if(ourInstance == null) {
            ourInstance = new BaseController();
        }
        return ourInstance;
    }

}
