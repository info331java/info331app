package no.uib.wastelad.engine;



import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

import no.uib.wastelad.R;
import no.uib.wastelad.activities.ModuleActivity;
import no.uib.wastelad.elements.Module;
import no.uib.wastelad.elements.RequirementsNotMetException;
import no.uib.wastelad.player.Player;

/**
 * Controller for the modules in the application. Class serves as a entry point for the views and engines to access the modules.
 *
 * @version 1.0
 * @author Rune Myklevoll
 * @since 03.12.2017
 */
public class ModuleController implements IController {

    private static final ModuleController instance = new ModuleController();
    private final ArrayList<Module> modules;
    private ModuleActivity activity;

    /**
     * Constructor for the BaseController. Private due to singleton pattern.
     */
    private ModuleController() {
        this.modules = new ArrayList<>();
        createModules();
    }

    private void createModules(){
        modules.add(new Module("Plastic Bottles", true,  20, 10, R.drawable.plastic, R.drawable.plasticl));
        modules.add(new Module("Glass Bottles", false, 30, 15, R.drawable.glass, R.drawable.glassl));
        modules.add(new Module("Waste", false,  50,  30, R.drawable.waste, R.drawable.wastel));
        modules.add(new Module("Gas", false, 80,  50, R.drawable.gas, R.drawable.gasl));
        modules.add(new Module("Toxic Waste", false, 100,  80, R.drawable.toxic, R.drawable.toxicl));
    }


    /**
     * Increment all modules by n steps
     * @param steps amount of steps
     */
    public void incrementAll(int steps) {
        for(Module module : modules) {
            if(module.increment(steps)) {
                collectFrom(module);
            }
        }
    }


    /**
     * Collect karma from a module
     * @param module the module
     */
    private void collectFrom(Module module) {
        int collected = module.collect();
        Player.getInstance().addCurrency(collected);
    }

    /**
     * Decide what to do when the button on the modules is clicked
     * @param module the module clicked
     */
    public void buttonClicked(Module module) {
        try {
            if(module.isUnlocked()) module.upgrade();
            else module.unlock();
            refreshAll();
        }
        catch(RequirementsNotMetException e) {
            activity.showRequirement(e);
        }
    }

    /**
     * Check for updates in the module and notify the bindings
     */
    private void refreshAll() {
        for(Module module : modules) {
            module.notifyChange();
        }
    }

    public void setActivity(AppCompatActivity activity) {
        this.activity = (ModuleActivity) activity;
    }

    public static ModuleController getInstance() {
        return instance;
    }



    public ArrayList<Module> getModules() {
        return modules;
    }
}
