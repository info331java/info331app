package no.uib.wastelad.engine;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import no.uib.wastelad.R;
import no.uib.wastelad.activities.MainActivity;
import no.uib.wastelad.player.Pedometer;
import no.uib.wastelad.player.Player;


/**
 * Engine for the pedometer controlling the step progress of the modules in the application. Class serves as a entry point for the views and engines to access the modules.
 * @version 1.0
 * @author Adrian Borgund
 * @since 09.11.2017
 */

public class PedometerEngine extends Service implements SensorEventListener {

    private SensorManager sensorManager;
    private Sensor sensor = null;
    private float steps;
    private float lastSteps;
    private Pedometer pedometer = Pedometer.getInstance();
    private boolean runningInForeground = false;
    private static final int NOTIF_ID = 707133707;
    private Player player = Player.getInstance();
    private ModuleController moduleController;

    /**
     * onStartCommand is called when the service is created and started.
     * @param intent the intent to run in the service
     * @param flags the flags to use. It is not used in this case
     * @param startId the start Id of the service
     * @return Service.START_STICKY integer representation of the service sticky
     */
   @Override
   public int onStartCommand(Intent intent, int flags, int startId){
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        moduleController = ModuleController.getInstance();
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_FASTEST);
        startNotificationForeground();
        lastSteps = 0;

        return Service.START_STICKY;
    }

    /**
     * Overriding the onSensorChanged to update values depending on the step counter values.
     * @param event the SensorEvent for the internal step counter sensor.
     */
    @Override
    public void onSensorChanged(SensorEvent event) {
        long timestamp = event.timestamp;
        float value = event.values[0];
        steps = value;

        pedometer.addData(timestamp,value);

        String text = "Steps: " + value;
        text = text.substring(0,text.length()-2);
        text += " Karma: " + player.getCurrency();

        updateNotification(text);
        player.updateSteps(value);

        if(lastSteps > 0) moduleController.incrementAll((int) (value - lastSteps));
        lastSteps = value;
    }

    /**
     * onDestroy is called when the service is shutdown by application shutdown.
     */
    @Override
    public void onDestroy() {
       super.onDestroy();
       killService();
    }


    /**
     * Maked the survice run in the foreground via the notification center on android.
     */
    private void startNotificationForeground() {
        if(!runningInForeground) {
            startForeground(NOTIF_ID, createNotification("Starting..."));
            runningInForeground = true;
        }
    }

    /**
     * A method to create a notification with a given text as description.
     * @param text the text to show in the description of the notification as a String
     * @return the Notification to show
     */
    private Notification createNotification(String text) {
        Intent notifyIntent = new Intent(this, PedometerEngine.class);
        PendingIntent pendingIntent = PendingIntent.getService(this, 100,notifyIntent,PendingIntent.FLAG_UPDATE_CURRENT);
        Intent resultIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingResultIntent = PendingIntent.getActivity(this, 0, resultIntent,PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder notifyBuilder = new NotificationCompat.Builder(this)
                .setContentTitle("Pedometer")
                .setContentText(text)
                .setSmallIcon(R.drawable.wastelad_logo)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.drawable.wastelad_logo))
                .setContentIntent(pendingIntent)
                .setTicker(text)
                .setContentIntent(pendingResultIntent);

        return notifyBuilder.build();
    }

    /**
     * A method to update the text description in the notification.
     * @param text the description to add in the notification.
     */
    private void updateNotification (String text) {
        final NotificationManager nm = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        Notification note = createNotification(text);

        nm.notify(NOTIF_ID, note);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    /**
     * A method to shut down the pedometer service.
     */
    public void killService() {
        sensorManager.unregisterListener(this);
        stopSelf();
    }

    public float getSteps() {
        return steps;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


}