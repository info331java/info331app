package no.uib.wastelad.elements;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import no.uib.wastelad.player.Player;


/**
 * Extension is an abstract superclass for the upgradable components of the Bases and Modules. Each extension has a separate
 * modifier variable which exists in order to generate custom algorithms in each instance.
 *
 * @version 1.0
 * @author Rune Myklevoll
 * @since 02.12.2017
 */
public abstract class Extension extends BaseObservable implements IPurchasable{
    private static final int COST_MODIFIER = 20;
    private static final float EFFECT_MODIFIER = 1.5f;

    private int price;
    private int level;
    private final int baseModifier;
    private int upgradeValue;

    /**
     * Constructor for the Extensions
     * @param baseModifier the extension modifier
     */
    public Extension(int baseModifier) {
        this.level = 1;
        this.baseModifier = baseModifier;
        generatePrice();
        generateValue();
    }

    /**
     * Attempt to upgrade the extension. Upgrade is initiated if player can afford it.
     * @throws RequirementsNotMetException when player cant afford the purchase
     */
    public void upgrade() throws RequirementsNotMetException {
        if(Player.getInstance().canAfford(this)) {
            buyUpgrade();
        }
        else throw new RequirementsNotMetException("You can't afford this purchase!");

    }

    /**
     * Perform extension upgrade and reevaluate value and price
     */
    private void buyUpgrade() {
        Player.getInstance().buy(this);
        level++;
        generateValue();
        generatePrice();
    }

    /**
     * Loads the extension level from phone storage and reevaluate value and price
     * @param level the level of the extension
     */
    protected void loadExtension(int level) {
        this.level = level;
        generateValue();
        generatePrice();
    }


    private void generateValue() {
        upgradeValue = (int) (baseModifier * level * EFFECT_MODIFIER);
    }

    private void generatePrice() {
        this.price = (level + (baseModifier/2)) * COST_MODIFIER;
    }

    @Bindable
    public int getLevel() {
        return level;
    }


    @Bindable
    public int getPrice() {
        return price;
    }

    @Bindable
    public int getUpgradeValue() {
        return upgradeValue;
    }
}
