package no.uib.wastelad.elements;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.location.Location;

import no.uib.wastelad.utilities.GeoTools;

/**
 * Model for the bases. Each base is representing a geolocation in Bergen. Players need to go to the physical location in order to unlock
 * or upgrade the base. The bases consists of a speed engine and a storage. Collecting karma from the storage does not require physical presence.
 *
 *
 * @version 1.0
 * @author Rune Myklevoll
 * @since 02.12.2017
 */
public class Base extends BaseObservable implements IGameElement {
    private static final int STORAGE_UPGRADE = 0;
    private static final int SPEED_UPGRADE = 1;
    private static final int MAXIMUM_DISTANCE_ALLOWED = 100;

    private final String baseName;
    private final BaseStorage storage;
    private final BaseSpeed speed;
    private boolean unlocked;
    private final Location location;
    private final int image;
    private final int lockedImage;


    /**
     * Creates an instance representing a Base
     * @param baseName name of the base
     * @param defaultStorage the amount of storage room for the bsae
     * @param unlocked whether the base is unlocked or not
     * @param longitude longitude of the base
     * @param latitude latitude of the base
     * @param image image shown when base is unlocked
     * @param lockedImage image shown when base is locked
     */
    public Base(String baseName, int defaultStorage, boolean unlocked, float longitude, float latitude, int image, int lockedImage) {
        this.baseName = baseName;
        this.storage = new BaseStorage(defaultStorage);
        this.speed = new BaseSpeed(1);
        this.unlocked = unlocked;
        this.location = GeoTools.buildLocation(longitude, latitude);
        this.image = image;
        this.lockedImage = lockedImage;
    }

    /**
     * Increment the accumulated karma in storage by one and notify the view
     */
    public void increment() {
        if(unlocked){
            storage.increment(speed.getUpgradeValue());
            notifyChange();
        }
    }

    /**
     * Collect accumulated karma from the base
     * @return accumulated karma
     */
    public int collect() {
        return storage.collect();
    }

    /**
     * Unlock the base if the player is within range of the base.
     * @param lastKnownLocation the last know location of the player(usually current position)
     * @throws RequirementsNotMetException
     */
    public void unlock(Location lastKnownLocation) throws RequirementsNotMetException {

        if(playerIsClose(lastKnownLocation)) {
            unlocked = true;
            notifyChange();
        }
    }

    /**
     * Extension one of the upgrades of the base if the player is within range and can afford it.
     * @param upgradeType which of the upgrades types the player has selected
     * @param lastKnownLocation the last know location of the player(usually current position)
     * @throws RequirementsNotMetException when player is too far away
     */
    public void upgrade(int upgradeType, Location lastKnownLocation) throws RequirementsNotMetException {
        if(playerIsClose(lastKnownLocation) ) {
            if (upgradeType == Base.SPEED_UPGRADE) speed.upgrade();
            else if (upgradeType == Base.STORAGE_UPGRADE) storage.upgrade();
        }

    }

    /**
     * Check if the player is in proximity of the base
     * @param lastKnownLocation the last know location of the player(usually current position)
     * @return true if player is close enough
     * @throws RequirementsNotMetException when player is too far away
     */
    private boolean playerIsClose(Location lastKnownLocation) throws RequirementsNotMetException {
        int distance = rangeToPlayer(lastKnownLocation);

        if(distance > MAXIMUM_DISTANCE_ALLOWED) {
            throw new RequirementsNotMetException("Too far away, (Distance: " + distance +")");
        }
        return true;
    }

    /**
     * Get the range between the base and the player
     * @param lastKnownLocation the last know location of the player(usually current position)
     * @return the range
     */
    private int rangeToPlayer(Location lastKnownLocation) {
        return (int) location.distanceTo(lastKnownLocation);
    }

    public void loadBase(int accumulated, int storageLevel, int speedLevel, boolean unlocked) {
        storage.loadBaseStorage(accumulated, storageLevel);
        speed.loadExtension(speedLevel);
        this.unlocked = unlocked;
    }

    public int[] saveBaseData() {
        int unlocked = this.unlocked ? 1: 0;
        return new int[]{storage.getAccumulated(), storage.getLevel(), speed.getLevel(), unlocked};
    }

    @Bindable
    public boolean isUnlocked() {
        return unlocked;
    }

    @Bindable
    public BaseStorage getStorage() {
        return storage;
    }

    @Bindable
    public Extension getSpeed() {
        return speed;
    }

    @Bindable
    public String getBaseName() {
        return baseName;
    }

    @Bindable
    public int getImage() {
        return image;
    }

    @Bindable
    public int getLockedImage() {
        return lockedImage;
    }


}
