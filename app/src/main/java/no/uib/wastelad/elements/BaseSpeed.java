package no.uib.wastelad.elements;

/**
 * BaseSpeed serves the upgrade of the bases deciding the speed a base gathers karma.
 *
 * @version 1.0
 * @author Rune Myklevoll
 * @since 02.12.2017
 */

class BaseSpeed extends Extension {

    /**
     * Constructor for the base speed generator
     * @param baseModifier how much the speed should be modified
     */
    public BaseSpeed(int baseModifier) {
        super(baseModifier);
    }
}
