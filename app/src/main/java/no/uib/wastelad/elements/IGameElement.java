package no.uib.wastelad.elements;

/**
 * Interface for the bases and modules
 *
 * @version 1.0
 * @author Rune Myklevoll
 * @since 02.12.2017
 */

public interface IGameElement {

    boolean isUnlocked();
    int getImage();
    int getLockedImage();

}
