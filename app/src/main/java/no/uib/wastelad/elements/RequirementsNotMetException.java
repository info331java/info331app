package no.uib.wastelad.elements;


/**
 * Custom exception used when the player is trying to buy or upgrade something without being able to afford it or is too far
 * away from a given location.

 * @version 1.0
 * @author Rune Myklevoll
 * @since 02.12.2017
 */
public class RequirementsNotMetException extends Exception {

    public RequirementsNotMetException(String message) {
        super(message);
    }
}
