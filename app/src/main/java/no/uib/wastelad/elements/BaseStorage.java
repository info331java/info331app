package no.uib.wastelad.elements;

import android.databinding.Bindable;

/**
 * BaseStorage serves the upgrade of the bases deciding the total capacity of the base and the current amount of karma collected.
 *
 * @version 1.0
 * @author Rune Myklevoll
 * @since 02.12.2017
 */

public class BaseStorage extends Extension {
    private int accumulated;

    /**
     * Constructor for the base speed generator
     * @param defaultStorage default storage space in base
     */
    public BaseStorage(int defaultStorage) {
        super(defaultStorage);
    }

    /**
     * Get the accumulated karma, return it and reset accumulated
     * @return accumulated karma
     */
    protected int collect() {
        int collected = accumulated;
        accumulated = 0;
        return collected;
    }

    /**
     * Increment the accumulated karma
     * @param ticksPerSec how to it should be incremented
     */
    protected void increment(float ticksPerSec) {
        accumulated += ticksPerSec;
        if(isFull()) {
            accumulated = getUpgradeValue();
        }
    }

    private boolean isFull() {
        return accumulated >= getUpgradeValue();
    }

    /**
     * Load the stored state of the base storage.
     * @param accumulated the accumulated value for the storage
     * @param level the level of the storage extension
     */
    protected void loadBaseStorage(int accumulated, int level) {
        super.loadExtension(level);
        if(accumulated > getUpgradeValue())
            this.accumulated = getUpgradeValue();
        else
            this.accumulated = accumulated;

    }


    @Bindable
    public int getAccumulated() {
        return accumulated;
    }

    @Bindable
    public String getProgressText () {
        if(!isFull()) return accumulated + " / " + getUpgradeValue();
        else return "-- Click to Collect --";
    }
}
