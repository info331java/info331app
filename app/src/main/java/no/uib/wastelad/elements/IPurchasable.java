package no.uib.wastelad.elements;

/**
 * Interface for purchasable items in the game, such as extensions and modules.
 *
 *
 * @version 1.0
 * @author Rune Myklevoll
 * @since 02.12.2017
 */
public interface IPurchasable {
    int getPrice();
}
