package no.uib.wastelad.elements;

/**
 * ModuleExtension serves the upgrade of the modules controlling how much karma is earned per iteration
 *
 * @version 1.0
 * @author Rune Myklevoll
 * @since 02.12.2017
 */

public class ModuleExtension extends Extension {
    public ModuleExtension(int baseModifier) {
        super(baseModifier);
    }
}
