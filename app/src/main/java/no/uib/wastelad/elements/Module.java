package no.uib.wastelad.elements;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import no.uib.wastelad.player.Player;


/**
 * Model for the modules. The modules are connected to the amount of steps a player takes.
 * Steps received by the sensor is essential returned as karma. The modules are upgradable which increases the
 * karma received.
 *
 *
 * @version 1.0
 * @author Rune Myklevoll
 * @since 02.12.2017
 */

public class Module extends BaseObservable implements IGameElement, IPurchasable {

    private final String name;
    private final int maxSteps;
    private final ModuleExtension upgrade;
    private final int imageInt;
    private final int lockedImageInt;
    
    private boolean unLocked;
    private int stepsProgress;


    /**
     * Creates an instance representing a Module
     * @param name name of the module
     * @param unLocked whether the base is unlocked
     * @param maxSteps max amount of steps per iteration
     * @param baseIncome base income per iteration
     * @param imageInt image shown when base is unlocked
     * @param lockedImageInt image show when base is locked
     */
    public Module(String name, boolean unLocked, int maxSteps, int baseIncome, int imageInt, int lockedImageInt) {
        this.name = name;
        this.unLocked = unLocked;
        this.stepsProgress = 1;
        this.maxSteps = maxSteps;
        this.upgrade = new ModuleExtension(baseIncome*2);
        this.imageInt = imageInt;
        this.lockedImageInt = lockedImageInt;
    }


    /**
     * Increase accumulated steps in the base by a given amount
     * @param steps amount of steps
     * @return true if full after increment
     */
    public boolean increment(int steps) {
       if(unLocked) {
           stepsProgress += steps;
           notifyChange();
           return isFull();
       }
       return false;
    }

    private boolean isFull() {
        return stepsProgress >= maxSteps;
    }

    /**
     * Attempt to upgrade the module
     * @throws RequirementsNotMetException
     */
    public void upgrade() throws RequirementsNotMetException {
        upgrade.upgrade();
    }

    /**
     * Attempt to unlock the module if its affordable
     * @throws RequirementsNotMetException
     */
    public void unlock() throws RequirementsNotMetException {
        Player player = Player.getInstance();
        if(player.canAfford(this)) {
            player.buy(this);
            unLocked = true;
        }
        else throw new RequirementsNotMetException("Can't afford this");
    }

    /**
     * Return karma earned and reset steps
     * @return karma earned
     */
    public int collect() {
       stepsProgress = stepsProgress - maxSteps;
       return upgrade.getUpgradeValue();

    }

    /**
     * Saves the module data for step progress, level and unlocked state.
     * @return int[] int array with [0] steps proress, [1] level and [2] and boolean where 0 = false and 1 = true
     */
    public int[] saveModuleData() {
        int unlocked = this.unLocked ? 1 : 0;
        return new int[]{stepsProgress, upgrade.getLevel(), unlocked};
    }

    /**
     * Method to load the stored state of a module.
     * @param stepsProgress the int progress towards the modules stepMax
     * @param level the int level of the module extension
     * @param unLocked the unlocked state as a boolean
     */
    public void loadModule(int stepsProgress, int level, boolean unLocked) {
        this.stepsProgress = stepsProgress;
        this.unLocked = unLocked;
        upgrade.loadExtension(level);
    }

    @Override
    public boolean isUnlocked() {
        return unLocked;
    }

    @Override
    public int getImage() {
        return imageInt;
    }

    @Override
    public int getLockedImage() {
        return lockedImageInt;
    }

    @Bindable
    public String getName() {
        return name;
    }

    @Bindable
    public int getMaxSteps() {
        return maxSteps;
    }


    @Bindable
    public Extension getUpgrade() {
        return upgrade;
    }

    @Bindable
    public int getStepsProgress() {
        return stepsProgress;
    }

    @Override @Bindable
    public int getPrice() {
        return (int) (upgrade.getPrice() * 4);
    }
}
