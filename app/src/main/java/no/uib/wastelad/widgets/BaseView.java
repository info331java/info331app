package no.uib.wastelad.widgets;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;

import no.uib.wastelad.databinding.BaseBinding;
import no.uib.wastelad.engine.BaseController;


import no.uib.wastelad.elements.Base;
import no.uib.wastelad.player.Player;

/**
 * View for the bases in the game. Layout is inflated based on a related xml layout.
 * The data model is connected using data binding.
 *
 * @version 1.0
 * @author Rune Myklevoll
 * @since 03.12.2017
 */

public class BaseView extends LinearLayout{

    private final Base base;
    private BaseBinding binding;

    /**
     * Constructor for the base view
     * @param context context of the view
     * @param base the base
     */
    public BaseView(Context context, Base base) {
        super(context);
        this.base = base;
        inflateView();
        setBinding();
    }

    private void inflateView() {
        LayoutInflater inflater =  (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        binding = BaseBinding.inflate(inflater, this, true);

    }

    private void setBinding() {
        binding.setBase(base);
        binding.setController(BaseController.getInstance());
        binding.setPlayer(Player.getInstance());
    }

    @BindingAdapter({"android:src"})
    public static void setImageViewResource(ImageView imageView, int resource) {
        imageView.setImageResource(resource);
    }
}
