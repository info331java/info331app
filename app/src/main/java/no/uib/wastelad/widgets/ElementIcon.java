package no.uib.wastelad.widgets;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import no.uib.wastelad.elements.IGameElement;

/**
 * Element icon used in the main activity. Currently only showing an image, but can be edited at a
 * later stage
 *
 * @version 1.0
 * @author Rune Myklevoll
 * @since 03.12.2017
 */

public class ElementIcon extends LinearLayout {

    public ElementIcon(Context context, IGameElement element) {
        super(context);

        ImageView image = createImage(element);
        RelativeLayout imageWrapper = new RelativeLayout(this.getContext());
        imageWrapper.addView(image);
        addView(imageWrapper);
        setupParams();

    }

    private ImageView createImage(IGameElement element) {
        ImageView image = new ImageView(getContext());
        image.setImageResource(getImageID(element));
        image.setScaleType(ImageView.ScaleType.FIT_CENTER);
        image.setLayoutParams(createImageParams());
        return image;
    }

    private int getImageID(IGameElement element) {
        return element.isUnlocked() ? element.getImage() : element.getLockedImage();
    }

    private RelativeLayout.LayoutParams createImageParams() {
        return new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    private void setupParams() {
        LinearLayout.LayoutParams params = new LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.weight = 1;
        setLayoutParams(params);
    }


}
