package no.uib.wastelad.widgets;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;


import no.uib.wastelad.databinding.ModuleBinding;
import no.uib.wastelad.engine.ModuleController;
import no.uib.wastelad.elements.Module;
import no.uib.wastelad.player.Player;

/**
 * View for the modules in the game. Layout is inflated based on a related xml layout.
 * The data model is connected using data binding.
 *
 * @version 1.0
 * @author Rune Myklevoll
 * @since 03.12.2017
 */
public class ModuleView extends LinearLayout {
    private final Module module;
    private ModuleBinding binding;

    /**
     * Constructor for the base view
     * @param context of the view
     * @param module the module
     */
    public ModuleView(Context context, Module module) {
        super(context);
        this.module = module;
        inflateView();
        setBindings();

    }

    private void inflateView() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        binding = ModuleBinding.inflate(inflater, this, true);


    }

    private void setBindings() {
        binding.setModule(module);
        binding.setController(ModuleController.getInstance());
        binding.setPlayer(Player.getInstance());
    }


    @BindingAdapter({"android:src"})
    public static void setImageViewResource(ImageView imageView, int resource) {
        imageView.setImageResource(resource);
    }


}
