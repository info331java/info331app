package no.uib.wastelad.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import no.uib.wastelad.R;

/**
 *  @author Felipe Sepulveda & Kim Nørve
 *  @version 1.0
 *
 *  SplashScreenActivity is the class that starts the splash screen and initiates the tutorial
 *  Player gets to skip or resume to the tutorial when Load Tutorial is pressed.
 */

public class SplashScreenActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 2000; //Splash screen is set to 2000ms (2sek)

    /**
     * onCreate runs when the activity starts up. Sets the the content view for
     * the splash screen. Runs delayScreenTimeout()
     * @param savedInstanceState previous saved state (Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        delayScreenAfterTimeout();
    }

    /**
     * Calls the superclass and proceed to the previous activity that was in Pause state.
     */
    @Override
    public void onResume() {
        super.onResume();
        delayScreenAfterTimeout();
    }


    /**
     * Creates a delay for the splash where its possible possible to resume to.
     * When Splash screen timer is done the tutorial initiates.
     */
    private void delayScreenAfterTimeout() {
        new Handler().postDelayed(new Runnable()
        {

            /**
             * Run starts the activity for Splash and Tutorial.
             */
            @Override
            public void run()
            {
                Intent homeIntent = new Intent(SplashScreenActivity.this, WelcomeActivity.class);
                startActivity(homeIntent);
            }
        }, SPLASH_TIME_OUT);
    }
}
