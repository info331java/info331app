package no.uib.wastelad.activities;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import no.uib.wastelad.R;
import no.uib.wastelad.slider.IntroPageAdapter;
import no.uib.wastelad.storage.DataStorage;

/**
 * @author Felipe Sepulveda & Kim Nørve
 * @version 1.0
 *
 * WelcomeActivity class is for the tutorial slides.
 * The class controls and adds the elements to the slides: the dots, skip/next/start, buttons, slides.
 * The tutorial is to help the player get an understanding of the game and how to play it.
 */

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {
    private ViewPager mPager;
    private final int numberOfSlides = 6;
    private IntroPageAdapter introPageAdapter;
    private ImageView[] dots;
    private Button nextButton, skipButton;
    private DataStorage ds;

    /**
     *
     * onCreate call the superclass and sets resource from the activity_welcome.xml.
     * Creates a new content from DataStorage
     * @param savedInstanceState previous saved state (Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ds = new DataStorage(getApplicationContext());
        checkIfUserHasIgnoredSplash();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        setupButtons();
        setupPager();

        createDots();
        updateDots(0);
    }

    /**
     *
     * onPageSelected checks if it's the last slide in the tutorial and changes NEXT button
     * to START and sets to SKIP button to invisible. If the player slides back from the last slide
     * SKIP button is set to visible and changes START to NEXT.
     * @param position Position set for dots (int).
     */
    @Override
    public void onPageSelected(int position)
    {
        updateDots(position);
        if (isLastSlide(position))
        {
            nextButton.setText("Start");
            skipButton.setVisibility(View.INVISIBLE);
        }
        else
            {
            skipButton.setVisibility(View.VISIBLE);
            nextButton.setText("Next");
        }

    }

    /**
     * Checks if the current slide is the last slide.
     * @param position sets position
     * @return position equals slides - 1.
     */
    private boolean isLastSlide(int position)
    {
        return position == numberOfSlides -1;
    }

    /**
     *
     * updateDots updates the dots for each time the player slides.
     * Sets dots to active (dark) if the slide position matches the dots, else default (light).
     * @param position sets the position for the dots (int)
     */
    private void updateDots(int position)
    {
        for(int i = 0; i < numberOfSlides; i++)
        {
            if(i == position) dots[i].setImageDrawable(getDrawable(R.drawable.active_dots));
            else dots[i].setImageDrawable(getDrawable(R.drawable.default_dots));
        }
    }

    /**
     *
     * Initiate activity for home screen in the game.
     * loadHome
     */
    private void loadHome()
    {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    /**
     *  checkIfUserHasIgnoredSplash checks if user has ignored tutorial after splash.
     *  Runs though the tutorial if not.
     *  Calls loadHome.
     */
    private void checkIfUserHasIgnoredSplash()
    {
        if(ds.isWelcomeScreenIgnored())
        {
            loadHome();
        }
    }

    /**
     * Action for the SKIP and NEXT button.
     * If NEXT button is clicked, load next slide.
     * Else SKIP button clicked rest of tutorial is ignored.
     * load home screen.
     * @param view the view
     */
    @Override
    public void onClick(View view)
    {
        if(view.equals(nextButton)) loadNextSlide();
        else if(view.equals(skipButton))
        {
            ds.saveWelcomeScreenIgnore(true);
            loadHome();
        }
    }

    /**
     * loadNextSlide checks if NEXT button is pressed.
     * Return the current slide + 1 from getNext() method.
     */
    private void loadNextSlide()
    {
        if(hasNext())
        {
            mPager.setCurrentItem(getNext());
        }
        else
        {
            ds.saveWelcomeScreenIgnore(true);
            loadHome();
        }
    }

    /**
     *
     * getNext returns the current slide + 1.
     * @return mPager.getCurrentItem() + 1  (Return current slide + 1)
     */
    private int getNext()
    {
        return mPager.getCurrentItem()+1;
    }


    private boolean hasNext()
    {
        return getNext() < numberOfSlides;
    }

    /**
     *
     * creates the dots for the Layout from dotsLayout.
     * Initiates dots as many as numberOfSlides.
     * Sets position to the current layout.
     * For each slide it
     */
    private void createDots()
    {
        LinearLayout dotLayout = (LinearLayout) findViewById(R.id.dotsLayout);
        this.dots = new ImageView[numberOfSlides];

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(4,0,4,0);

        for(int i = 0; i < numberOfSlides; i++) {
            ImageView dot = new ImageView(this);

            this.dots[i] = dot;
            dotLayout.addView(dot, params);

        }
    }

    /**
     * setUpButton takes the resource from bnNext and bnSkip, adds listener to both of them.
     */
    private void setupButtons()
    {
        nextButton = (Button) findViewById(R.id.bnNext);
        skipButton = (Button) findViewById(R.id.bnSkip);
        nextButton.setOnClickListener(this);
        skipButton.setOnClickListener(this);
    }

    /**
     *
     * setupPager takes resource to mPager value and adds it to tutorial.
     */
    private void setupPager()
    {
        mPager = (ViewPager) findViewById(R.id.viewPager);
        introPageAdapter = new IntroPageAdapter(this);
        mPager.setAdapter(introPageAdapter);
        mPager.addOnPageChangeListener(this);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
    {

    }


    @Override
    public void onPageScrollStateChanged(int state)
    {

    }
}