package no.uib.wastelad.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.widget.LinearLayout;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import no.uib.wastelad.R;
import no.uib.wastelad.databinding.ActivityBasesBinding;
import no.uib.wastelad.elements.Base;
import no.uib.wastelad.engine.BaseController;
import no.uib.wastelad.player.Player;
import no.uib.wastelad.utilities.GoogleClientCallbacks;
import no.uib.wastelad.widgets.BaseView;


/**
 * The base activity is one of currently three activities in the application. The activity uses an inflated layout from a separate xml-file.
 * The bases are added programmatically based on the separate class BaseView. BaseActivity is also in charge of connecting to the
 * google apis in order to retrieve the players geolocation.
 *
 * @version 1.0
 * @author Rune Myklevoll
 * @since 02.12.2017
 */
public class BasesActivity extends CoreActivity {

    private GoogleApiClient googleClient;
    private GoogleClientCallbacks googleClientCallbacks;
    private final static int REQUEST_LOCATION = 1;
    private ActivityBasesBinding binding;

    /**
     * onCreate runs when the activity is starting up.
     * This includes inflating the layout, setting up player bindings and adding the bases.
     * @param savedInstanceState Previous saved state (nullable)
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        googleClientCallbacks = new GoogleClientCallbacks();
        inflateLayout();
        bindPlayerToView();
        configureEngine();
        addBases();
        setupGoogleClient();
    }

    private void inflateLayout() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_bases);
    }

    private void bindPlayerToView() {
        binding.setPlayer(Player.getInstance());
    }
    /**
     * Setup the engines needed for the bases to work
     */
    private void configureEngine() {
        BaseController.getInstance().setActivity(this);
    }

    /**
     * Add bases to the activity view. The bases are gathered from the BaseController and programmatically added
     * to the activity view each time the activity is started.
     */
    private void addBases() {
        LinearLayout innerScrollView = (LinearLayout) findViewById(R.id.inner);
        for(Base base : BaseController.getInstance().getBases()) {
            innerScrollView.addView(new BaseView(getApplicationContext(), base));


        }
    }

    /**
     * Creates an instance of the google api client and connects it.
     */
    private void setupGoogleClient() {
        this.googleClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(googleClientCallbacks)
                .addOnConnectionFailedListener(googleClientCallbacks)
                .addApi(LocationServices.API)
                .build();

        googleClient.connect();
    }


    /**
     * Get the last know location of the player (usually the current location).
     * User got to grant access in order for this to work.
     * Lint suppressed since android doesn't seem to like the structure of permission request.
     * @return the location of the player
     */
    @SuppressLint("MissingPermission")
    public Location getLastKnownLocation() {
        if (!permissionGranted()){
            requestLocationPermission();
        }
        return LocationServices.FusedLocationApi.getLastLocation(googleClient);
    }

    /**
     * Check if user has granted the application access to the coarse or fine location.
     * @return true if access is allowed
     */
    private boolean permissionGranted() {
        return userHasGrantedAccessTo(Manifest.permission.ACCESS_COARSE_LOCATION) || userHasGrantedAccessTo(Manifest.permission.ACCESS_FINE_LOCATION);
    }

    /**
     * Check if user has granted the application to a specific permission
     * @param permissionType type of the permission
     * @return true if granted
     */
    private boolean userHasGrantedAccessTo(String permissionType) {
        return ActivityCompat.checkSelfPermission(this, permissionType) == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Asks the user for permission to the fine location of the device
     */
    private void requestLocationPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
    }



}
