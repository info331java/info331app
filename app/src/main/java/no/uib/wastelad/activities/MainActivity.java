package no.uib.wastelad.activities;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import no.uib.wastelad.R;
import no.uib.wastelad.elements.Base;
import no.uib.wastelad.databinding.ActivityMainBinding;
import no.uib.wastelad.engine.BaseController;
import no.uib.wastelad.engine.ModuleController;
import no.uib.wastelad.elements.Module;
import no.uib.wastelad.player.Player;
import no.uib.wastelad.widgets.ElementIcon;
import no.uib.wastelad.storage.DataStorage;

/**
 * The main activity only serves as an overview screen currently, showing personlig information like steps taken,
 * the bases and modules and their related statuses.
 * The intent is to expand this activity as the amount of various data increases.
 *
 * @version 1.0
 * @author Rune Myklevoll
 * @since 02.12.2017
 */
public class MainActivity extends CoreActivity {

    /**
     * onCreate runs when the activity is starting up.
     * This includes setting up bindings and adding the modules and bases.
     * @param savedInstanceState Previous saved state (nullable)
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupBinding();
        setupModules();
        setupBases();
    }

    private void setupBinding() {
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setPlayer(Player.getInstance());
    }


    private void setupModules() {
        LinearLayout moduleWrapper = (LinearLayout) findViewById(R.id.moduleWrapper);
        for(Module module : ModuleController.getInstance().getModules()) {
            moduleWrapper.addView(new ElementIcon(this, module));
        }
    }

    private void setupBases() {
        LinearLayout baseWrapper = (LinearLayout) findViewById(R.id.baseWrapper);
        for(Base base : BaseController.getInstance().getBases()){
            baseWrapper.addView(new ElementIcon(this, base));
        }
    }

    /**
     * Makes it possible to load the tutorial slides again after completing it.
     * @param view the view calling the method
     */
    public void loadSlides(View view) {
        new DataStorage(getApplicationContext()).saveWelcomeScreenIgnore(false);
        startActivity(new Intent(this,WelcomeActivity.class));
    }



}

