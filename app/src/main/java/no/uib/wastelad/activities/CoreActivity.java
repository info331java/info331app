package no.uib.wastelad.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import no.uib.wastelad.elements.RequirementsNotMetException;
import no.uib.wastelad.engine.MainEngine;
import no.uib.wastelad.engine.PedometerEngine;
import no.uib.wastelad.storage.DataStorage;

/**
 * Superclass for the various activities, responsibilities include changing tabs within the application when the
 * menu is pressed, starting pedometer and loading game.
 *
 * @version 1.0
 * @author Rune Myklevoll
 * @since 02.12.2017
 */

public abstract class CoreActivity extends AppCompatActivity {

    private DataStorage dataStorage;

    /**
     * onCreate runs when the activity is starting up.
     * Starts the main engine and the pedometer engine and loads the game within the data storage
     * @param savedInstanceState Previous saved state (nullable)
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startPedometer();
        MainEngine.getInstance().start();
        loadGame();

    }

    /**
     * Show a toast with an error message upon illegal action inside the application (such as buying something you
     * cant afford)
     * @param e Exception caught
     */
    public void showRequirement(RequirementsNotMetException e) {
        Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
    }

    /**
     * Tells the pedometer service to startTimedEngine. All activities in the application should include
     * this service in order to progress regardless of where you are within the application.
     */
    private void startPedometer() {
        Intent pedometerIntent = new Intent(getApplicationContext(), PedometerEngine.class);
        startService(pedometerIntent);
    }


    /**
     * Loads the game upon activity creation
     */
    private void loadGame() {
        if(dataStorage == null) {
            dataStorage = new DataStorage(getApplicationContext());
        }
        dataStorage.loadGame();
    }

    /**
     * Saves the game when the application closes and change of tab.
     */
    @Override
    public void onPause() {
        super.onPause();
        if(dataStorage == null) {
            dataStorage = new DataStorage(getApplicationContext());
        }
        dataStorage.saveGame();
    }

    /**
     * Change tab in the application
     * @param activityClass the class the application should change to
     */
    private void startMenuActivity(Class activityClass) {
        Intent intent = new Intent(this, activityClass);
        startActivity(intent);
        finish();
    }

    /**
     * Swap to main activity
     * @param view clicked
     */
    public void home(View view) {
        startMenuActivity(MainActivity.class);
    }

    /**
     * Swap to module activity
     * @param view clicked
     */
    public void modules(View view) {
        startMenuActivity(ModuleActivity.class);
    }

    /**
     * Swap to base activity
     * @param view clicked
     */
    public void bases(View view) {
        startMenuActivity(BasesActivity.class);
    }

   }

