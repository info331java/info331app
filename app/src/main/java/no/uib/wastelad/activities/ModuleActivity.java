package no.uib.wastelad.activities;

import android.animation.ValueAnimator;
import android.databinding.DataBindingUtil;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;


import no.uib.wastelad.R;
import no.uib.wastelad.databinding.ActivityGameBinding;
import no.uib.wastelad.engine.ModuleController;
import no.uib.wastelad.elements.Module;
import no.uib.wastelad.player.Player;
import no.uib.wastelad.widgets.ModuleView;


/**
 * The module activity is one of currently three activities in the application. The activity uses an inflated layout from a separate xml-file.
 * The modules are added programmatically based on the separate class ModuleView.
 *
 * @version 1.0
 * @author Adrian Borund
 * @since 02.12.2017
 */
public class ModuleActivity extends CoreActivity implements  ValueAnimator.AnimatorUpdateListener{
    private ImageView backGroundOne, backGroundTwo, backGroundThree, backGroundFour;

    /**
     * onCreate runs when the activity is starting up.
     * This is setting up the bindings configures the engine and adding the modules and starting the animation in the activity.
     * @param savedInstanceState Previous saved state (nullable)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupBinding();
        configureEngine();
        setUpModulesInLayout();
        startAnimation();
    }

    private void setupBinding() {
        ActivityGameBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_game);
        binding.setPlayer(Player.getInstance());
    }

    private void configureEngine() {
        ModuleController.getInstance().setActivity(this);
    }

    private void setUpModulesInLayout() {
        LinearLayout layout = (LinearLayout) findViewById(R.id.moduleWrapper);
        for(Module module : ModuleController.getInstance().getModules()) {
            layout.addView(new ModuleView(getApplicationContext(), module));
        }
    }

    private void startAnimation() {
        startPlayerAnimation();
        setupBackGround();
        setupAnimator();
    }

    /**
     * Method for setting up and starting the animation of the player sprite.
     */
    private void startPlayerAnimation() {
        ImageView animationImageView = (ImageView) findViewById(R.id.animationView);
        animationImageView.setBackgroundResource(R.drawable.running_animation);

        AnimationDrawable anim = (AnimationDrawable) animationImageView.getBackground();
        anim.start();
    }
    /**
     * Method for setting up the different backgrounds used in the scrolling background animation behind the player sprite.
     */
    private void setupBackGround() {
        backGroundOne = (ImageView) findViewById(R.id.background_one);
        backGroundTwo = (ImageView) findViewById(R.id.background_two);
        backGroundThree = (ImageView) findViewById(R.id.background_three);
        backGroundFour = (ImageView) findViewById(R.id.background_four);
    }

    /**
     * Setting up the animator with variables like duration and repeat count for the background animation.
     */
    private void setupAnimator() {
        ValueAnimator animator = ValueAnimator.ofFloat(1.0f, 0.0f);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setInterpolator(new LinearInterpolator());
        animator.setDuration(25000L);
        animator.addUpdateListener(this);
        animator.start();
    }

    /**
     * Overriding the onAnimationUpdate method to be able to have the different ImageViews scroll next to each other and repeat.
     * @param animation the ValueAnimator to animate
     */
    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        final float progress = (float) animation.getAnimatedValue();
        final float width = backGroundOne.getWidth();
        final float translationX = width * 3 * progress ;
        backGroundOne.setTranslationX(translationX);
        backGroundTwo.setTranslationX(translationX - width);
        backGroundThree.setTranslationX(translationX - (width * 2));
        backGroundFour.setTranslationX(translationX - (width * 3));
    }
}
