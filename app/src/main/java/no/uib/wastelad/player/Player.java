package no.uib.wastelad.player;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import no.uib.wastelad.BR;
import no.uib.wastelad.elements.IPurchasable;

/**
 * Represents a player in the game.
 * Created by Rune on 27.09.2017.
 */

public class Player extends BaseObservable {
    private static final Player ourInstance = new Player();
    private long currency;
    private float steps;


    private Player() {
        currency = 20L;
        steps = 0;
    }

    public static Player getInstance() {
        return ourInstance;
    }


    /**
     * Buy item
     * @param item to be bought
     */
    public void buy(IPurchasable item) {
        currency -= item.getPrice();
        notifyChange();
    }

    public void addCurrency(long currencyToAdd) {
        this.currency += currencyToAdd;
       notifyPropertyChanged(BR.currency);
    }

    /**
     * Check if player can afford a purchasable item
     * @param item to be purchased
     * @return true if player can afford
     */
    public boolean canAfford(IPurchasable item) {
        return currency >= item.getPrice();
    }


    @Bindable
    public long getCurrency() {

        return currency;
    }
    public void updateSteps(float steps) {
        if(this.steps < steps) {
            this.steps = steps;
           notifyPropertyChanged(BR.steps);
        }
    }

    @Bindable
    public int getSteps() {
        return (int) this.steps;
    }

    public void setSteps(float steps) {
        this.steps = steps;
    }

    public void setCurrency(long currency) {
        this.currency = currency;
    }
}
