package no.uib.wastelad.player;

import java.util.ArrayList;
import no.uib.wastelad.utilities.DataTuple;

/**
 * Pedometer for the application used by the PedometerEngine to store and update the steps and timestamps from the sensor.
 * Is implemented as a singleton.
 * @version 1.0
 * @author Adrian Borgund
 * @since 09.11.2017
 */
public class Pedometer {
    private static final Pedometer ourInstance = new Pedometer();
    private float totalSteps;
    private long lastTimestamp;
    private float lastSteps;
    private float savedSteps;
    private ArrayList<DataTuple> data;

    public static Pedometer getInstance() {
        return ourInstance;
    }

    private Pedometer() {
        totalSteps = 0;
        savedSteps = 0;
        lastTimestamp = 0;
        data = new ArrayList<>();
    }

    /**
     * Creates a tuple of timestamp and steps.
     * @param timestamp long
     * @param steps float
     * @return DataTuple with timestamp and steps.
     */
    private DataTuple makeTuple(long timestamp, float steps) {
        return new DataTuple(timestamp,steps);
    }

    /**
     * A method to calculate the totals steps of the player.
     */
    private void calculateTotalSteps() {
        if (totalSteps < lastSteps) {
            totalSteps = lastSteps;
            savedSteps = lastSteps;
        } else if(savedSteps < lastSteps) {
            totalSteps += lastSteps - savedSteps;
            savedSteps = lastSteps;
        } else {
            savedSteps = lastSteps;
        }
    }

    /**
     * A method to add steps and timestamps to the arraylist.
     * @param timestamp the timestamp from the sensor registering steps.
     * @param steps the steps from the step counter sensor.
     */
    public void addData(long timestamp, float steps) {
        DataTuple tuple = makeTuple(timestamp,steps);
        data.add(tuple);
        lastSteps = steps;
        calculateTotalSteps();
        lastTimestamp = timestamp;
    }

    /**
     * A method to return the last entered tuple
     * @return last enreted DataTuple, null if none exist
     */
    public DataTuple peekTuple() {
        if(data.size() > 0) {
            return data.get(data.size()-1);
        }
        return null;
    }

    /**
     * A method to see the last entered steps
     * @return float the last entered steps, -1 if none exist
     */
    public float peekSteps() {
        if(data.size() > 0) {
            return peekTuple().getSteps();
        }
        else {
            return -1;
        }
    }

    /**
     * A method to see the last entered timestamp
     * @return long the last entered timestamo, 0 if none exist
     */
    public long peekTimestamp() {
        if(data.size() > 0) {
            return peekTuple().getTimestamp();
        }
        else {
            return 0L;
        }
    }

    public void clearData() {
        this.data = new ArrayList<>();
    }

    public float getTotalSteps() {
        return totalSteps;
    }

    public long getLastTimestamp() {
        return lastTimestamp;
    }

    public void setTotalSteps(float totalSteps) {
        this.totalSteps = totalSteps;
    }

    public void setLastTimestamp(long lastTimestamp) {
        this.lastTimestamp = lastTimestamp;
    }

    public float getLastSteps() {
        return lastSteps;
    }

    public void setLastSteps(float lastSteps) {
        this.lastSteps = lastSteps;
    }

    public ArrayList<DataTuple> getData() {
        return data;
    }

    public float getSavedSteps() {
        return savedSteps;
    }

    public void setSavedSteps(float savedSteps) {
        this.savedSteps = savedSteps;
    }
}


